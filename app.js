'use strict';

const app = require('express')();
const express = require('express');
const path = require('path');
const checkIp4 = require('./src/checkip4_controller')
// socket.io
var server = require('http').Server(app); 
// TODO not sure we even need CORS here.  Validate
const io = require('socket.io')(server, { origins: '*:*'});

module.exports = app; // for testing

var config = {
  ipListUrl: 'https://iplists.firehol.org/files/firehol_level1.netset',
  ipListName: 'firehol_level1',
  refreshInterval: 1000 * 10 * 60 // every hour
};

app.use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => res.render('pages/index'))

app.get('/checkIp4/:ip', function (req, res) {
  checkIp4.checkIp4Http(req, res, config);
});

app.get('/ping', function (req, res) {
  res.send('pong');
});

io.on('connection', function (socket) {
  socket.on('checkIp4Request',function (data) {
    checkIp4.checkIp4Socket(data, config, socket);
  });
});

checkIp4.init(config).then(function () {
  var port = process.env.PORT || 5000;
  // app.listen(port);
  server.listen(port);
  console.log('server started at port: ' + port);
}).catch(error => console.error('server failed to start because blacklist could not be loaded into IP tree'));