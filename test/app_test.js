const supertest = require('supertest')
const assert = require('assert');
const app = require('../app');

describe('api', function () {
    this.timeout(5000);

    it('should pong', function (done) {
        supertest(app)
            .get('/ping')
            .expect(200)
            .end(done);
    });

    it('should allow non-blacklisted IP', function (done) {
        supertest(app)
            .get('/checkIp4/54.230.22.170')
            .expect(200)
            .end(function (err, res) {
                responseJson = JSON.parse(res.text);
                assert.equal(responseJson.isValid, true);
                assert.equal(responseJson.sourceList, undefined);
                done();
            });
    });

    it('should deny blacklisted IP', function (done) {
        supertest(app)
            .get('/checkIp4/10.1.1.1')
            .expect(200)
            .end(function (err, res) {
                responseJson = JSON.parse(res.text);
                assert.equal(responseJson.isValid, false);
                assert.equal(responseJson.sourceList, 'firehol_level1');
                done();
            });
    });

    it('should 400 when invalid IP', function (done) {
        supertest(app)
            .get('/checkIp4/1O.1.l.1S')
            .expect(400)
            .end(done());
    });

    it('should 400 when no IP', function (done) {
        supertest(app)
            .get('/checkIp4')
            .expect(400)
            .end(done());
    });
});