'use strict';

const assert = require('assert');
const ipBinary = require('../../src/util/ipv4_to_binary');

describe('ipv4_to_binary', function () {

    describe('isIpAddress', function () {
        it('should return true when IP valid', function (done) {
            assert.equal(ipBinary.isIpAddress('192.168.1.1'), true);
            assert.equal(ipBinary.isIpAddress('10.0.0.1'), true);
            done();
        });

        it('should return false when IP invalid', function (done) {
            assert.equal(ipBinary.isIpAddress('192.168.1'), false);
            assert.equal(ipBinary.isIpAddress('ZZZ.a.1.34'), false);
            done();
        });

        it('should return false for IP range', function (done) {
            assert.equal(ipBinary.isIpAddress('120.46.0.0/15'), false);
            done();
        });
    })

    describe('isIpRange', function () {
        it('should return true when CIDR IP Range', function (done) {
            assert.equal(ipBinary.isIpRange('120.46.0.0/15'), true);
            assert.equal(ipBinary.isIpRange('91.200.164.0/24'), true);
            done();
        });

        it('should return false when range invalid', function (done) {
            assert.equal(ipBinary.isIpRange('120.46.A.0/15'), false);
            assert.equal(ipBinary.isIpRange('120.46.0.0/33'), false);
            done();
        });

        it('should return false for IP adress', function (done) {
            assert.equal(ipBinary.isIpRange('120.46.0.1'), false);
            done();
        });
    })

    it('should split CIDR notation into address and range values', function (done) {
        assert.deepEqual(ipBinary.splitCidrRange('192.168.1.1/16'), ['192.168.1.1', 16]);
        assert.deepEqual(ipBinary.splitCidrRange('120.46.0.0/24'), ['120.46.0.0', 24]);
        assert.deepEqual(ipBinary.splitCidrRange('192.168.1.1'), ['192.168.1.1', 32]);
        done();
    });

    it('should convert IP address to binary', function (done) {
        ipBinary.addressToBinary('0.0.0.0', function (binary) {
            assert.equal(binary, '00000000000000000000000000000000')
        });
        ipBinary.addressToBinary('88.11.100.15', function (binary) {
            assert.equal(binary, '01011000000010110110010000001111')
        });
        ipBinary.addressToBinary('255.255.255.255', function (binary) {
            assert.equal(binary, '11111111111111111111111111111111')
        });
        done();
    });

    it('should convert CIDR range to binary', function (done) {
        ipBinary.addressToBinary('192.168.1.1/16', function (binary) {
            assert.equal(binary, '1100000010101000')
        });
        ipBinary.addressToBinary('59.254.0.0/15', function (binary) {
            assert.equal(binary, '001110111111111')
        });
        ipBinary.addressToBinary('41.138.164.0/22', function (binary) {
            assert.equal(binary, '0010100110001010101001')
        });
        done();
    });

    it('should throw Error on invalid input string', function (done) {
        try {
            ipBinary.addressToBinary('192.168.0.1//16', function (binary) {
                assert.equal(binary, '1100000010101000')
            });
        } catch (Error) {
            done();
        }
    });
});