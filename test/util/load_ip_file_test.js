'use strict';

const assert = require('assert');
const loadIpFile = require('../../src/util/load_ip_file');

describe('load ip file controller', function () {
    this.timeout(5000);

    it('should load an IP file from URL', function (done) {

        // TODO ideally this would spin up a local resource and load - this test has external dependendies and is
        // subject to false postivies/negatives if the specified file changes
        loadIpFile.loadIpFileUrlToTree('https://iplists.firehol.org/files/firehol_level1.netset')
            .then(function (loadedTree) {
                loadedTree.query('5.8.37.1', result => assert.equal(result, true));
                loadedTree.query('54.230.22.213', result => assert.equal(result, false));

                done();
            }).catch(error => console.log(error));;
    });

    it('should reject a bad URL', function (done) {
        loadIpFile.loadIpFileUrlToTree('jcain://my:custom:resource:type')
            .then(function (loadedTree) {
                console.log('Failed to reject promise for invalid URL')
            }).catch(error => done());;
    });

});