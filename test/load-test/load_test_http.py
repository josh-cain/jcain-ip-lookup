
from locust import HttpLocust, TaskSet, task
import urllib
import logging
import random

class HttpCaller(TaskSet):

    @task(100)
    def call_with_valid_ip(self):
        ipQuery = str(random.randint(0, 255)) + "." + str(random.randint(0, 255)) + "." + str(random.randint(0, 255)) + "." + str(random.randint(0, 255))
        self.client.get("/checkIp4/" + ipQuery, name="/checkIp4/IP")

    @task(1)
    def call_ping(self):
        self.client.get("/ping/")

class ApiCaller(HttpLocust):
    task_set = HttpCaller
    min_wait = 0
    max_wait = 0