'use strict';

const assert = require('assert');
const tree = require('../../src/model/binary_tree');

describe('binary_tree', function () {

    describe('with one layer', function () {
        it('should reflect empty', function (done) {
            var binaryTree = new tree.BinaryTree(() => false, () => false);
            assert.equal(binaryTree.query('1'), false);
            assert.equal(binaryTree.query('0'), false);
            done();
        });

        it('should reflect single entry', function (done) {
            var binaryTree = new tree.BinaryTree(() => false, () => false);
            binaryTree.insert('1');
            assert.equal(binaryTree.query('1'), true);
            assert.equal(binaryTree.query('0'), false);
            done();
        });

        it('should count less significant places as covered', function (done) {
            var binaryTree = new tree.BinaryTree(() => false, () => false);
            binaryTree.insert('1');
            assert.equal(binaryTree.query('111'), true);
            assert.equal(binaryTree.query('10101'), true);
            done();
        });

        it('should reflect full tree', function (done) {
            var binaryTree = new tree.BinaryTree(() => false, () => false);
            binaryTree.insert('0');
            binaryTree.insert('1');
            assert.equal(binaryTree.query('1'), true);
            assert.equal(binaryTree.query('0'), true);
            done();
        });

    });

    describe('with two layers', function () {
        it('should reflect single entry', function (done) {
            var binaryTree = new tree.BinaryTree(() => false, () => false);
            binaryTree.insert('11');
            assert.equal(binaryTree.query('11'), true);
            assert.equal(binaryTree.query('10'), false);
            assert.equal(binaryTree.query('01'), false);
            assert.equal(binaryTree.query('00'), false);
            done();
        });

        it('should reflect multiple entries', function (done) {
            var binaryTree = new tree.BinaryTree(() => false, () => false);
            binaryTree.insert('11');
            binaryTree.insert('00');
            assert.equal(binaryTree.query('11'), true);
            assert.equal(binaryTree.query('10'), false);
            assert.equal(binaryTree.query('01'), false);
            assert.equal(binaryTree.query('00'), true);
            done();
        });

        it('should reflect less significant places as covered', function (done) {
            var binaryTree = new tree.BinaryTree(() => false, () => false);
            binaryTree.insert('1');
            binaryTree.insert('00');
            assert.equal(binaryTree.query('11'), true);
            assert.equal(binaryTree.query('10'), true);
            assert.equal(binaryTree.query('01'), false);
            assert.equal(binaryTree.query('00'), true);
            done();
        });

        it('should ignore queries that are more specific than the tree has to be', function (done) {
            var binaryTree = new tree.BinaryTree(() => false, () => false);
            binaryTree.insert('1');
            binaryTree.insert('01');
            assert.equal(binaryTree.query('11'), true);
            assert.equal(binaryTree.query('10'), true);
            assert.equal(binaryTree.query('01'), true);
            assert.equal(binaryTree.query('00'), false);
            assert.equal(binaryTree.query('11001010101110100101011'), true);
            done();
        });

    });

    describe('with three layers', function () {
        it('should reflect single entry', function (done) {
            var binaryTree = new tree.BinaryTree();
            binaryTree.insert('101');
            assert.equal(binaryTree.query('111'), false);
            assert.equal(binaryTree.query('110'), false);
            assert.equal(binaryTree.query('101'), true);
            assert.equal(binaryTree.query('100'), false);
            assert.equal(binaryTree.query('011'), false);
            assert.equal(binaryTree.query('010'), false);
            assert.equal(binaryTree.query('001'), false);
            assert.equal(binaryTree.query('000'), false);
            done();
        });

        it('should reflect multiple entries', function (done) {
            var binaryTree = new tree.BinaryTree();
            binaryTree.insert('101');
            binaryTree.insert('011');
            binaryTree.insert('001');
            assert.equal(binaryTree.query('111'), false);
            assert.equal(binaryTree.query('110'), false);
            assert.equal(binaryTree.query('101'), true);
            assert.equal(binaryTree.query('100'), false);
            assert.equal(binaryTree.query('011'), true);
            assert.equal(binaryTree.query('010'), false);
            assert.equal(binaryTree.query('001'), true);
            assert.equal(binaryTree.query('000'), false);
            done();
        });

        it('should reflect less significant places as covered', function (done) {
            var binaryTree = new tree.BinaryTree(() => false, () => false);
            binaryTree.insert('1');
            assert.equal(binaryTree.query('111'), true);
            assert.equal(binaryTree.query('110'), true);
            assert.equal(binaryTree.query('101'), true);
            assert.equal(binaryTree.query('100'), true);
            assert.equal(binaryTree.query('011'), false);
            assert.equal(binaryTree.query('010'), false);
            assert.equal(binaryTree.query('001'), false);
            assert.equal(binaryTree.query('000'), false);
            done();
        });
    });

    describe('with 32 layers', function () {
        it('should reflect single entry', function (done) {
            var binaryTree = new tree.BinaryTree();
            binaryTree.insert('10110110110100111001010111001010');
            assert.equal(binaryTree.query('10110110110100111001010111001001'), false);
            assert.equal(binaryTree.query('10110110110100111001010111001010'), true);
            assert.equal(binaryTree.query('10110110110100111001010111001011'), false);
            done();
        });

        it('should reflect multiple entries', function (done) {
            var binaryTree = new tree.BinaryTree();
            binaryTree.insert('10110110110100111001010111001010');
            binaryTree.insert('11010011100101011100101011001101');
            assert.equal(binaryTree.query('10110110110100111001010111001001'), false);
            assert.equal(binaryTree.query('10110110110100111001010111001010'), true);
            assert.equal(binaryTree.query('10110110110100111001010111001011'), false);
            assert.equal(binaryTree.query('11010011100101011100101011001100'), false);
            assert.equal(binaryTree.query('11010011100101011100101011001101'), true);
            assert.equal(binaryTree.query('11010011100101011100101011001110'), false);
            done();
        });

        it('should reflect less significant places as covered', function (done) {
            var binaryTree = new tree.BinaryTree();
            binaryTree.insert('100');
            assert.equal(binaryTree.query('10010101110010100110110110110000'), true);
            assert.equal(binaryTree.query('10110110110100111001010111001010'), false);
            assert.equal(binaryTree.query('10110110110100111001010111001011'), false);
            done();
        });
    });

    describe('error conditions', function () {
        describe('on insert', function () {
            it('should not insert undefined string', function (done) {
                try {
                    var binaryTree = new tree.BinaryTree();
                    binaryTree.insert(undefined);
                } catch (Error) {
                    done();
                }
            });

            it('should not insert blank string', function (done) {
                try {
                    var binaryTree = new tree.BinaryTree();
                    binaryTree.insert(undefined);
                } catch (Error) {
                    done();
                }
            });

            it('should not insert non-binary character', function (done) {
                try {
                    var binaryTree = new tree.BinaryTree();
                    binaryTree.insert('!');
                } catch (Error) {
                    done();
                }
            });

            it('should not insert non-binary string', function (done) {
                try {
                    var binaryTree = new tree.BinaryTree();
                    binaryTree.insert('ASDF');
                } catch (Error) {
                    done();
                }
            });
        });
    });

});