'use strict';

module.exports.BinaryTree = BinaryTree;

/**
 * Tree structure meant to represent binary representations of IP addresses and ranges.
 */
function BinaryTree() {
    this.zeroQuery = (binaryString) => false;
    this.oneQuery = (binaryString) => false;
    this.zeroBranch = undefined;
    this.oneBranch = undefined;
}

/**
 * Determines whether or not a given binary string is represented by this node or its children.
 * 
 * @param binaryString: string of ones and zeroes (I.E. '011011011')
 * @returns true if a match for the binary query string if found.  false otherwise.
 * @throws Error if the binaryString is undefined, or shorter than the tree
 */
BinaryTree.prototype.query = function (binaryString) {
    if (binaryString === undefined || binaryString.length === 0) {
        throw new Error('attempted to query BinaryTree with undefined or empty string');
    }
    var head = this.head(binaryString);
    var tail = this.tail(binaryString);

    if (head === '0') {
        return this.zeroQuery(tail);
    } else if (head === '1') {
        return this.oneQuery(tail);
    } else {
        throw new Error('attempted to insert BinaryTree node with a nonbinary string: ' + binaryString);
    }
}

/**
 * Expands the tree as necessary to represent a match on the given binary string.  Note that binary strings given of a
 * length of less than the depth of the tree (I.E. '101' in the case of a 32-node-deep tree) will be considered a range,
 * and will register a match with all binary children (and halt evaluation).
 * 
 * @param binaryString: string of ones and zeroes (I.E. '011011011')
 * @throws Error if the binaryString is undefined, or shorter than the tree
 */
BinaryTree.prototype.insert = function (binaryString) {
    if (binaryString === undefined || binaryString.length === 0) {
        throw new Error('attempted to insert BinaryTree node with undefined or empty string');
    }
    var head = this.head(binaryString);
    var tail = this.tail(binaryString);

    if (tail.length === 0) {
        // termination case, early terminators always win since they represent larger ranges
        if (head === '0') {
            this.zeroQuery = (binaryString) => true;
        } else if (head === '1') {
            this.oneQuery = (binaryString) => true;
        } else {
            throw new Error('attempted to insert BinaryTree node with a nonbinary string');
        }
    } else {
        // standard case for non-end nodes
        if (head === '0') {
            if (this.zeroBranch == undefined) {
                this.zeroBranch = new BinaryTree();
                this.zeroQuery = (binaryString) => this.zeroBranch.query(binaryString);
            }

            this.zeroBranch.insert(tail);
        } else if (head === '1') {
            if (this.oneBranch == undefined) {
                this.oneBranch = new BinaryTree();
                this.oneQuery = (binaryString) => this.oneBranch.query(binaryString);
            }

            this.oneBranch.insert(tail);
        } else {
            throw new Error('attempted to insert BinaryTree node with a nonbinary string');
        }
    }
}

/**
 * Gets first element of the string
 */
BinaryTree.prototype.head = function (string) {
    if (string === undefined || string.length == 1) {
        return string;
    }

    return string.substring(0, 1);
}

/**
 * Gets all but the first element of the string
 */
BinaryTree.prototype.tail = function (string) {
    if (string === undefined) {
        return string;
    } else if (string.length == 1) {
        return '';
    }

    return string.substring(1, string.length);
}