'use strict';

const tree = require('./binary_tree');
const toBinary = require('../util/ipv4_to_binary');

module.exports.IpTree = IpTree;

/**
 * Convenience tree structure, backed by a BinaryTree that simply offers IP address + Range conversion as part
 * of the data structure.  Note that since address -> binary parsing involves callbacks, the use of this structure
 * will be slightly different than the backing BinaryTree.
 */
function IpTree() {
    this.delegate = new tree.BinaryTree();
}

/**
 * @see BinaryTree
 * @callback result be invoked with boolean 'true' or 'false' based on whether or not the string was found
 */
IpTree.prototype.query = function (ipString, callback) {
    toBinary.addressToBinary(ipString, binaryString => callback(this.delegate.query(binaryString)));
}

/**
 * @see BinaryTree
 */
IpTree.prototype.insert = function (ipString) {
    toBinary.addressToBinary(ipString, binaryString => this.delegate.insert(binaryString));
}