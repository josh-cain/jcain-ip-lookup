'use strict';

const Promise = require('promise');
const load = require('./util/load_ip_file');

module.exports = {
    getIpTree
};

var ipTree;

/**
 * Responsible for getting the IP tree.  A form of lazy loading is used in which the initial load and timer
 * refresh setup will not take place until the first request hits.
 * 
 * @param {*} config app config object, expected to contain ipListUrl, ipListName, and refreshInterval
 * @returns a Promise resolving to the loaded tree.
 */
function getIpTree(config) {
    return new Promise(function (resolve, reject) {
        if (ipTree === undefined) {
            console.log('initializing blacklist IP from source')
            load.loadIpFileUrlToTree(config.ipListUrl)
                .then(function (initializedTree) {
                    console.log('blacklist successfully initialized')
                    ipTree = initializedTree;

                    // set up timer
                    setInterval(function () {
                        console.log('refreshing IP blacklist from source')
                        load.loadIpFileUrlToTree(config.ipListUrl)
                            .then(newTree => ipTree = newTree)
                            .catch(error => console.error('Failed to refresh IP blacklist from source: ' + error));
                    }, config.refreshInterval);

                    resolve(ipTree);
                }).catch(error => reject('unable to initialize IP tree: ' + error));
        } else {
            resolve(ipTree);
        }
    });
}

