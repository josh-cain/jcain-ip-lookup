'use strict';

const ipv4 = require('node-ipv4')
const leftPad = require('left-pad')

// shamelessly pulled from http://blog.markhatton.co.uk/2011/03/15/regular-expressions-for-ip-addresses-cidr-ranges-and-hostnames/
var ipRegex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;
var cidrRegex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/(3[0-2]|[1-2][0-9]|[0-9]))$/;

module.exports = {
    isIpAddress,
    isIpRange,
    isConvertible,
    splitCidrRange,
    addressToBinary
};

/**
 * Determines whether or not a given string is a valid IP address
 * 
 * @param {*} inputIp String representing an IP address.  I.E. '192.168.1.1'
 */
function isIpAddress(inputIp) {
    return ipRegex.test(inputIp);
}

/**
 * Determines whether or not a given string is a valid CIDR range
 * 
 * @param {*} inputIp String representing a CIDR block.  I.E. '41.71.171.0/24'
 */
function isIpRange(inputIp) {
    return cidrRegex.test(inputIp);
}

/**
 * Determines whether or not a given string is a valid IP address or CIDR range
 * 
 * @param {*} inputIp String representing IP address or CIDR block
 */
function isConvertible(inputIp) {
    return inputIp !== undefined && (isIpAddress(inputIp) || isIpRange(inputIp));
}

/**
 * Splits an CIDR declaration into discrete parts - the address and offset.
 * 
 * @param {*} inputIp IP address or CIDR range.  I.E. '192.168.1.1' or '41.71.171.0/24'.
 * @returns Array of two elements, IP address and CIDR range offset.  I.E. '41.71.171.0/24' becomes ['41.71.171.0', 24].  If an regular IP address
 *      is given, a CIDR offset of '32' will be given.  I.E. '192.168.1.1' becomes ['192.168.1.1', 32].
 * @throws Error if the inputIP given is undefined
 */
function splitCidrRange(inputIp) {
    if (!isConvertible(inputIp)) {
        throw new Error('attempted to split an IP into address space and range with an invalid string: ' + inputIp);
    }
    var splitIpAndRange = inputIp.split(/\//);

    if (splitIpAndRange.length === 1) {
        splitIpAndRange = splitIpAndRange.concat(32);
    } else {
        splitIpAndRange[1] = parseInt(splitIpAndRange[1]);
    }

    return splitIpAndRange;
}

/**
 * converts a standard IPv4 address or CIDR range to binary representation string
 * 
 * @param {*} inputIp IPv4 address given as string (I.E. '192.168.1.1', or '41.71.171.0/24') 
 * @callback binaryAddress will be handed to the callback as a string
 * @throws Error if the string cannot be properly parsed as an IPv4 address or range.
 */
function addressToBinary(inputIp, callback) {
    var ipAndCidrRange = splitCidrRange(inputIp);

    ipv4.parse(ipAndCidrRange[0], ipAndCidrRange[1], (error, subnet) => {
        if (error) {
            throw Error('unable to parse IP address and CIDR range: ' + ipAndCidrRange)
        }

        if (subnet.cidr == 32) {
            var binaryAddress = leftPad(subnet.address.binary, 32, '0');
        } else {
            // FireHOL always uses first entry in subnet, but node-ipv1 bumps such that each octet > 0.  Not sure why.
            // this fixes the subnet.address.binary's auto-bump from .0 -> .1
            var binaryAddress = leftPad(subnet.first.binary, 32, '0').substring(0, subnet.cidr);
        }

        return callback(binaryAddress);
    });
}