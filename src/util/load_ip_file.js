'use strict';

const Http = require('request');
const Promise = require('promise');
const isUrl = require('is-url');
const tree = require('../model/ip_tree');

module.exports = {
    loadIpFileUrlToTree,
    retrieveIpFile
};

/**
 * retrieves the given .ipset or .netset file found at the url, and loads a fresh binary tree
 * 
 * Note tha the current implemenation loads the file into memory.  Sufficient for relatively small file sizes,
 * but will need to swap out for streaming implementation for larger ones.
 * 
 * @param {*} url location for the .ipset or .netset file
 * @returns a Promise that resolves to a BinaryTree object
 */
function loadIpFileUrlToTree(url) {
    return new Promise(function (resolve, reject) {
        retrieveIpFile(url)
            .then(body => transformIpFileToTree(body))
            .then(tree => resolve(tree))
            .catch(error => reject(error));
    });
}

/**
 * Gets the file content at the specified URL
 * 
 * @param {*} url locatoin for the file
 * @returns a Promise that resolves to the body content 
 */
function retrieveIpFile(url) {
    return new Promise(function (resolve, reject) {
        if (!isUrl(url)) {
            reject('URL supplied for file is invalid: ' + url);
        } else {
            Http.get(url, function (error, response, body) {
                if (error) {
                    reject('Error attempting to retrieve file from url ${url}: ${error}');
                } else if (response.statusCode != 200) {
                    reject('Non-200 status when attempting to retrieve ${url}: ${response.statusCode}');
                }

                resolve(body);
            });
        }
    });
}

/**
 * Takes the file content string and uses it to load an IpTree
 * 
 * @param {*} ipFileContent String representing the .ipset or .netset file content
 * @returns a Promise that resolves to an IpTree
 */
function transformIpFileToTree(ipFileContent) {
    return new Promise(function (resolve, reject) {

        var lines = ipFileContent.split(/\n/);
        var ipTree = new tree.IpTree();
        lines.forEach(element => {
            if ('#' === element[0] || !element) {
                return;
            }
            ipTree.insert(element);
        });

        resolve(ipTree);
    });
}