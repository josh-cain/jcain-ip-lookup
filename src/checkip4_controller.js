'use strict';

const listController = require('./list_refresh_controller');
const convert = require('./util/ipv4_to_binary');

module.exports = {
    init,
    checkIp4Http,
    checkIp4Socket
}

function init(config) {
    return new Promise(function (resolve, reject) {
        listController.getIpTree(config)
            .then(resolve())
            .catch(error => reject('Unable to initialize checkip4 controller: ' + error));
    });
}

/**
 * Performs the IP check operation over HTTP
 * 
 * @param {*} req HTTP request
 * @param {*} res HTTP response
 * @param {*} config app config object, expected to contain ipListUrl, ipListName, and refreshInterval
 */
function checkIp4Http(req, res, config) {
    var requestedIp = req.params.ip;

    if (convert.isConvertible(requestedIp)) {
        listController.getIpTree(config).then(function (tree) {
            tree.query(requestedIp, function (foundMatch) {
                res.json(buildResponseBody(foundMatch, config.ipListName));
            });
        }).catch(error => res.error('Error attempting to perform IP lookup'));
    } else {
        res.statusMessage = "Invalid input IP";
        res.status(400).end();
    }
}

/**
 * Performs the IP check operation of socket.io (websockets)
 * 
 * @param {P} inputData raw input from 'checkIp4Request' message, expected to be IP address to check
 * @param {*} config app config object, expected to contain ipListUrl, ipListName, and refreshInterval
 * @param {*} socket socket on which to send return 'checkIp4Response' message
 */
function checkIp4Socket(inputData, config, socket) {
    // TODO could generatlize the lookup steps, simply providing response callbacks for the various protocols.  DRY.
    if (convert.isConvertible(inputData)) {
        listController.getIpTree(config).then(function (tree) {
            tree.query(inputData, function (foundMatch) {
                socket.emit('checkIp4Response', JSON.stringify(buildResponseBody(foundMatch, config.ipListName)));
            });
        }).catch(error => socket.emit('checkIp4Response', 'ERROR: ' + error));
    } else {
        socket.emit('checkIp4Response', 'ERROR: Invalid IP Address String');
    }
}

/**
 * Populates JSON object with standardized 'isValid' and (if error) list name
 * 
 * @param {*} foundMatch false if IP was found to be on blacklist.  true otherwise.
 * @param {*} listName name of list used for blacklist search.
 */
function buildResponseBody(foundMatch, listName) {
    var responseBody = {
        'isValid': !foundMatch
    };
    if (foundMatch) {
        responseBody.sourceList = listName;
    }
    return responseBody;
}